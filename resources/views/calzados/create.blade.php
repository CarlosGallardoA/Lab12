@extends('layouts.layout')
@section('title','Crear')
@section('content')
<form action="{{ url('/calzados') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="calzado" class="form-label">{{ 'Modelo' }}</label>
        <input type="text" name="calzado" id="calzado" class="form-control">
    </div>
    <div class="mb-3">
        <label for="marca" class="form-label">{{ 'Marca' }}</label>
        <input type="text" name="marca" class="form-control" id="marca">
    </div>
    <div class="mb-3">
        <label for="precio" class="form-label">{{ 'Precio' }}</label>
        <input type="text" name="precio" class="form-control" id="precio">
    </div>
    <div class="mb-3">
        <label for="img" class="form-label">{{ 'Imagen' }}</label>
        <input type="file" name="img" class="form-control" id="img" value="">
    </div>
    <input type="submit" class="btn btn-primary mt-3" name="" id="" value="Agregar">
</form>
@endsection