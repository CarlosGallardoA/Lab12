@extends('layouts.layout')
@section('title','ShoesApp')
@section('content')
<a href="{{ url('/calzados/create') }}" class="btn btn-primary">Agregar</a>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Imagen</th>
      <th scope="col">Modelo</th>
      <th scope="col">Marca</th>
      <th scope="col">Precio</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($calzados as $calzado )
        <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <td><img src="{{ asset('storage').'/'.$calzado->img }}" alt="" width="70" height="80" class="img-thumbnail img-fluid"></td>
            <td>{{ $calzado->calzado }}</td>
            <td>{{ $calzado->marca }}</td>
            <td>{{ $calzado->precio }} $</td>
            <td>
                <a href="{{ url('/calzados/'.$calzado->id.'/edit') }}" class="btn btn-warning">Comprar</a>
                <form action="{{ url('/calzados/'.$calzado->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Eliminar ?')" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
    @endforeach
  </tbody>
</table>
{{ $calzados->links() }}
@endsection