@extends('layouts.layout')
@section('content')
<form action="{{ url('/calzados/'.$calzado->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label for="calzado" class="form-label">{{ 'Modelo' }}</label>
        <input type="text" name="calzado" id="calzado" class="form-control" disabled value="{{ $calzado->calzado }}">
    </div>
    <div class="mb-3">
        <label for="marca" class="form-label">{{ 'Marca' }}</label>
        <input type="text" name="marca" class="form-control" id="marca" disabled value="{{ $calzado->marca }}">
    </div>
    <div class="mb-3">
        <label for="precio" class="form-label">{{ 'Precio' }}</label>
        <input type="text" name="precio" class="form-control" id="precio" disabled value="{{ $calzado->precio }}">
    </div>
    <div class="form-group md-3">
        <label for="talla">talla</label>
        <select id="talla" name="talla" class="form-control">
            <option>35.5</option>
            <option>36.5</option>
            <option>37.5</option>
            <option>37.5</option>
            <option>38.5</option>
        </select>
    </div>
    <div class="form-group md-3">
        <label for="tipo">Tipo</label>
        <select id="tipo" name="tipo" class="form-control">
            <option>Normal</option>
            <option>Botines</option>
        </select>
    </div>
    <div class="form-group md-3">
        <label for="genero">Genero</label>
        <select id="genero" name="genero" class="form-control">
            <option>Hombre</option>
            <option>Mujer</option>
            <option>Unisex</option>
        </select>
    </div>
    <div class="form-group md-3">
        <label for="edades">Edad</label>
        <select id="edades" name="edades" class="form-control">
            <option>Adulto</option>
            <option>Niño</option>
        </select>
    </div>
    <div class="mb-3">
        <label for="img" class="form-label">{{ 'Imagen' }}</label>
        @if (isset($calzado->img))
            <img src="{{ asset('storage').'/'.$calzado->img }}" alt="" width="200">
        @endif
        <input type="file" name="img" class="form-control" id="img" value="">
    </div>
    <input type="submit" class="btn btn-primary" name="" id="" value="Actualizar">
</form>
@endsection