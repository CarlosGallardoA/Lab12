<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCalzadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calzados', function (Blueprint $table) {
            $table->id();
            $table->string('calzado');
            $table->string('img');
            $table->string('tipo')->nullable();
            $table->decimal('talla',8,2)->nullable();
            $table->string('marca');
            $table->string('genero')->nullable();
            $table->string('edades')->nullable();
            $table->decimal('precio',8,2);
            $table->timestamp('f_pago')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('t_pago')->nullable();
            $table->decimal('monto',8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calzados');
    }
}
