<?php

namespace App\Http\Controllers;

use App\Models\Calzado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CalzadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calzados = Calzado::paginate(5);
        return view('calzados.index', compact('calzados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('calzados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = request()->except('_token'); 
        if($request->hasFile('img')){
            $datos['img'] = $request->file('img')->store('uploads','public');
        }
        Calzado::insert($datos);
        return redirect('calzados');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Calzado  $calzado
     * @return \Illuminate\Http\Response
     */
    public function show(Calzado $calzado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Calzado  $calzado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $calzado = Calzado::findOrFail($id);
        return view('calzados.edit',compact('calzado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Calzado  $calzado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos = request()->except('_token','_method');
        if($request->hasFile('img')){
            $calzado = Calzado::findOrFail($id);
            Storage::delete('public/'.$calzado->img);
            $datos['img'] = $request->file('img')->store('uploads','public');
        }
        Calzado::where('id','=',$id)->update($datos);
        $calzado = Calzado::findOrFail($id);
        return redirect('calzados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Calzado  $calzado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $calzado = Calzado::findOrFail($id);
        if(Storage::delete('public/'.$calzado->img)){
            Calzado::destroy($id);
        }
        return redirect('calzados');
    }

    public function ventas()
    {
        $calzados = Calzado::paginate(5);
        return view('calzados.ventas', compact('calzados'));
    }
}
